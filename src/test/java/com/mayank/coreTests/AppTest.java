package com.mayank.coreTests;

import com.mayank.core.App;
import org.junit.Test;
import org.junit.Assert;
public class AppTest {
    @Test
    public void testHex() {
        String expected = "123456";
        Assert.assertEquals(expected,App.convertInput256SHA("123456"));
    }
}